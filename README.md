# Basic App with React + Django

This is a test case to play with the combination of Django and React to create a web app. It is based mostly on [this tutorial](https://github.com/diogosouza/django-react-logrocket) on [LogRocket](https://blog.logrocket.com/using-react-django-create-app-tutorial/). The tutorial creates an app to list students from a database, including the ability to change, add, or delete database entries.

I then used [this article](https://dev.to/cesareferrari/how-to-display-error-messages-in-a-react-application-3g48) to add error messages to the form if not all fields are filled out, and [this article](https://allegra9.medium.com/react-form-validations-286590d26b6f) to figure out how to add a custom error message.


## Getting started

If you want to use the code in this repo, I highly recommend using the above sources to create it for yourself (it doesn't take long). If you don't want that, something like this should also work.

First, create a Python virtual environment:

```
python3 -m venv logrocket_env
```

Then activate it. On Windows I'm doing it like this:

```
.\logrocket_env\Scripts\activate
```

On other operating systems, it should work like that:

```
source logrocket_env/bin/activate
```

Now you can clone this repo into your `venv`.

## Setting up Django

Install Django and some tools:

```
pip install django djangorestframework django-cors-headers
```

Then `cd` into the *django_react_proj* directory and start the server (since you cloned my repo, you can skip all the migration steps you'd usually need to do first to get the Django project running):

```
python manage.py runserver
```

You can check if the API works by browsing to http://localhost:8000/api/students/.

## Setting up React

Now `cd` into the *students-fe* directory and install the relevant Node modules before starting the server:

```
npm install
npm start
```

Your browser should now automtically open to http://localhost:3000/ and show you the React frontend.